# How to run

To run and enter container shell:
```
docker-compose build && \
docker-compose up -d && \
docker exec -it $(docker ps -aq -f name=sisdis_proxy) /bin/bash
```

Access at localhost:5020