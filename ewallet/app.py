import json
import os
import requests
import sys

from contextlib import contextmanager
from flask import Flask, Config, request, jsonify
from flask.views import View
from flask_injector import FlaskInjector
from flask_sqlalchemy import SQLAlchemy
from injector import inject
from sqlalchemy.exc import SQLAlchemyError

### Global Variables
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
db = SQLAlchemy(app)

### Schema
class Account(db.Model):
    __tablename__ = 'accounts'

    user_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    money = db.Column(db.Integer)

    def __repr__(self):
        return "<Account(user_id='%s', name='%s', money='%s')>" % (
            self.user_id, self.name, self.money)

### Error Handler
class AppErrorCode:
    UNREGISTERED_USER = -1
    FAILED_QUORUM = -2
    REMOTE_HOST_FAILURE = -3
    DATABASE_ERROR = -4
    TRANSFER_INPUT_ERROR = -5
    UNDEFINED_ERROR = -99

class AppError(Exception):
    def __init__(self, code):
        self.code = code
    
    def __str__(self):
        return str(self.code)
    
    def to_dict(self, key):
        response = dict()
        response[key] = self.code
        return response

def error_response(error):
    response_key = response_key_from_url(request.path)
    if response_key is None:
        response_key = 'message'

    return jsonify(error.to_dict(response_key))

@app.errorhandler(AppError)
def common_error_handler(error):
    print("Application error occured: {}".format(error), file=sys.stderr)
    return error_response(error)

@app.errorhandler(SQLAlchemyError)
def db_error_handler(error):
    print("Database error occured: {}".format(error), file=sys.stderr)
    error = AppError(AppErrorCode.DATABASE_ERROR)
    return error_response(error)

@app.errorhandler(Exception)
def undefined_error_handler(error):
    print("Undefined error occured: {}".format(error), file=sys.stderr)
    error = AppError(AppErrorCode.UNDEFINED_ERROR)
    return error_response(error)

### Helper
def response_key_from_url(path):
    if path == '/ewallet/ping':
        return 'pingReturn'
    elif path == '/ewallet/register':
        return 'registerReturn'
    elif path == '/ewallet/transfer':
        return 'transferReturn'
    elif path == '/ewallet/getSaldo':
        return 'saldo'
    elif path == '/ewallet/getTotalSaldo':
        return 'saldo'
    else:
        return None

def get_hosts_mapping():
    if os.environ['DEPLOYMENT_ENV'] == 'dev':
        return [{'npm': "web" + str(d), 'ip': 'web' + str(d)} for d in range(8)]
    else:
        r = requests.get('http://172.22.0.222/lapors/list.php')
        if not r:
            raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)
        return r.json()

def user_id_to_host(user_id):
    hosts = get_hosts_mapping()
    host = None
    for entry in hosts:
        if entry['npm'] == user_id:
            host = entry['ip']
            break
    
    if host is None:
        raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)
    
    return host

def get_saldo_call(host_user_id, user_id):
    host = user_id_to_host(host_user_id)
    print("Try host {} for saldo call!".format(host), file=sys.stderr)
    r = requests.post('http://{}/ewallet/getSaldo'.format(host), json={
        "user_id": user_id
    })
    if not r:
        raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)

    res = r.json()
    return int(res['saldo'])

def register_call(host_user_id, user_id, name):
    host = user_id_to_host(host_user_id)
    print("Try host {} for register call!".format(host), file=sys.stderr)
    r = requests.post('http://{}/ewallet/register'.format(host), json={
        "user_id": user_id,
        "nama": name
    })

    if not r:
        raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)

    res = r.json()
    if res['registerReturn'] == 1:
        return True
    else:
        return False

def transfer_call(host_user_id, user_id, name, amount):
    if amount < 0 or amount > 1000000000:
        return False
    user = get_account(user_id)
    if not user or (user and user.money < amount):
        return False

    saldo = get_saldo_call(host_user_id, user_id)
    if saldo >= 0:
        pass
    elif saldo == -1:
        if not register_call(host_user_id, user_id, name):
            return False
    else:
        return False
    
    host = user_id_to_host(host_user_id)
    print("Try host {} for transfer call!".format(host), file=sys.stderr)
    r = requests.post('http://{}/ewallet/transfer'.format(host), json={
        "user_id": user_id,
        "nilai": amount
    })
    if not r:
        raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)
    
    res = r.json()
    if res['transferReturn'] == 1:
        user.money = user.money - amount
        db.session.commit()
        return True
    else:
        return False

def get_total_saldo_call(user_id):
    hosts = get_hosts_mapping()
    total_saldo = 0
    for entry in hosts:
        host = entry['ip']
        r = requests.post('http://{}/ewallet/getSaldo'.format(host), json={
            "user_id": user_id
        })
        if r and r.json()['saldo'] >= 0:
            total_saldo = total_saldo + int(r.json()['saldo'])
        elif r and r.json()['saldo'] == -1:
            print("User {} unregistered at {}".format(user_id, host), file=sys.stderr)
        else:
            raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)
    
    return total_saldo

def request_total_saldo_call(user_id):
    host = user_id_to_host(user_id)
    print("Try host {} for total saldo call!".format(host), file=sys.stderr)
    r = requests.post('http://{}/ewallet/getTotalSaldo'.format(host), json={
        "user_id": user_id
    })
    if not r:
        raise AppError(AppErrorCode.REMOTE_HOST_FAILURE)

    return r.json()

def quorum_checker():
    hosts = get_hosts_mapping()
    hosts = hosts[:8]
    responded = 0
    for entry in hosts:
        host = entry['ip']
        r = requests.post('http://{}/ewallet/ping'.format(host))
        if r and r.json()['pingReturn'] == 1:
            print("Host {} responded!".format(host), file=sys.stderr)
            responded = responded + 1
    
    return responded

def get_account(user_id):
    return Account.query.filter_by(user_id=user_id).first()

### Routes
@app.route("/ewallet/getSaldo", methods=["POST"])
def get_saldo(db: SQLAlchemy):
    body = request.json
    user_id = str(body['user_id'])

    user = get_account(user_id)
    if not user:
        raise AppError(AppErrorCode.UNREGISTERED_USER)
    elif quorum_checker() < 5:
        raise AppError(AppErrorCode.FAILED_QUORUM)
    else:
        return jsonify({"saldo": user.money})

@app.route("/ewallet/getTotalSaldo", methods=["POST"])
def get_total_saldo(db: SQLAlchemy):
    body = request.json
    user_id = str(body['user_id'])

    user = get_account(user_id)
    if not user:
        raise AppError(AppErrorCode.UNREGISTERED_USER)
    elif quorum_checker() < 8:
        raise AppError(AppErrorCode.FAILED_QUORUM)
    elif user_id == os.environ['DOMICILE_USER_ID']:
        total_saldo = get_total_saldo_call(user_id)
        return jsonify({"saldo": total_saldo})
    else:
        return jsonify(request_total_saldo_call(user_id))

@app.route("/ewallet/ping", methods=["POST"])
def ping(db: SQLAlchemy):
    return jsonify({"pingReturn": 1})

@app.route("/ewallet/register", methods=["POST"])
def register(db: SQLAlchemy):
    body = request.json
    user_id = str(body['user_id'])
    nama = str(body['nama'])

    if quorum_checker() < 5:
        raise AppError(AppErrorCode.FAILED_QUORUM)
    else:
        user = get_account(user_id)
        if not user:
            user = Account(user_id=user_id, name=nama, money=0)
            db.session.add(user)
        else:
            user.name = nama
        db.session.commit()
    
    return jsonify({"registerReturn": 1})

@app.route("/ewallet/transfer", methods=["POST"])
def transfer(db: SQLAlchemy):
    body = request.json
    user_id = str(body['user_id'])
    nilai = int(body['nilai'])

    user = get_account(user_id)
    if not user:
        raise AppError(AppErrorCode.UNREGISTERED_USER)
    elif quorum_checker() < 5:
        raise AppError(AppErrorCode.FAILED_QUORUM)
    elif nilai < 0 or nilai > 1000000000:
        raise AppError(AppErrorCode.TRANSFER_INPUT_ERROR)
    else:
        user.money = user.money + nilai
        db.session.commit()
        return jsonify({"transferReturn": 1})

### Dependency injection
def configure(binder):
    binder.bind(
        SQLAlchemy,
        to=db,
        scope=request,
    )

if __name__ == "__main__":
    # Migration
    db.create_all()
    db.session.commit()

    user = get_account(os.environ['DOMICILE_USER_ID'])
    if not user:
        myself = Account(user_id=os.environ['DOMICILE_USER_ID'], name='Aldi Fahrezi', money=1000000000)
        db.session.add(myself)
        db.session.commit()

    # Run
    FlaskInjector(app=app, modules=[configure])
    if 'EWALLET_DEPLOYMENT_PORT' not in os.environ:
        app.run(host="0.0.0.0", port=80, debug=True)
    else:
        app.run(host="0.0.0.0", port=int(os.environ['EWALLET_DEPLOYMENT_PORT']), debug=True)