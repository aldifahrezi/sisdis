import json
import os
import sqlalchemy
import sys

import app

from io import StringIO

### Messages
greeting_message = """
Welcome to kantor cabang {}!
""".format(os.environ['DOMICILE_USER_ID'])

login_message = """
Masukkan User ID Anda untuk login (registrasi secara otomatis apabila User
ID Anda belum terdaftar):
"""

operation_message = """
Berikut adalah operasi-operasi yang dapat anda lakukan dengan memasukkan
perintah yang bersesuaian:
1. Transfer - Untuk mentransfer duit Anda dari cabang ini pada cabang yang
   lainnya.
   perintah: transfer <jumlah> <tujuan_cabang>
   contoh:   transfer 5000000 1506690623

2. List cabang - Untuk mengetahui list cabang yang dapat menjadi tujuan
   pengiriman
   perintah: list_cabang

3. Cek saldo Anda - Untuk mengetahui total saldo Anda, baik di kantor cabang
   ini dan secar keseluruhan
   perintah: cek_saldo

4. Help - Menampilkan ulang pesan kumpulan perintah ini
   perintah: help

5. Logout
   perintah: logout
"""

if __name__ == "__main__":
    # Redirect error message
    sys.stderr = StringIO();

    print(greeting_message)
    print(login_message, end="")
    user_id = str(input())
    user = app.get_account(user_id)
    if not user:
        print("Anda belum teregistrasi di kantor ini, silakan masukkan nama lengkap Anda:")
        name = str(input())
        user = app.Account(user_id=user_id, name=name, money=0)
        app.db.session.add(user)
        app.db.session.commit()
        print("Registrasi sukses!")
    
    print("Selamat datang {}!".format(user.name))   
    print(operation_message)
    
    while True:
        app.db.session.expire_all()
        print("Silakan masukkan perintah yang diinginkan:")
        try:
            queries = input().split(' ')
            command = queries[0]
            if command == 'logout':
                print("Terima kasih telah menggunakan kantor cabang ini!")
                sys.exit(0)
            elif command == 'cek_saldo':
                print("Saldo Anda di kantor cabang ini adalah: {}".format(user.money))
                print("Saldo Anda di seluruh kantor cabang adalah: {}".format(app.get_total_saldo_call(user.user_id)))
            elif command == 'list_cabang':
                print(json.dumps(app.get_hosts_mapping(), sort_keys=True, indent=4))
            elif command == 'transfer':
                amount = int(queries[1])
                dest = str(queries[2])
                if amount < 0 or amount > 1000000000:
                    print("Maaf, jumlah tidak bisa negatif atau lebih dari 1 milyar")
                    continue
                if amount > user.money:
                    print("Maaf, saldo Anda di kantor cabang ini tidak mencukupi.")
                    continue

                if app.transfer_call(dest, user.user_id, user.name, amount):
                    print("Transfer berhasil dilakukan!")
                else:
                    print("Transfer gagal dilakukan.")
            elif command == "help":
                print(operation_message)
            elif command == "":
                continue
            else:
                print("Maaf perintah tidak dikenali.")
        except Exception:
           print("Maaf terjadi kesalahan pada sistem. Silakan masukkan perintah kembali") 

    
