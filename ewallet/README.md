# How to run

To run and enter container shell (web0 service's shell)
```
docker-compose build && \
docker-compose up -d && \
docker exec -it $(docker ps -aq -f name=ewallet_web | head -n 1) /bin/bash
```

To run whole services
```
docker-compose build && \
docker-compose up -d
```

Access at localhost:[5000-5007]

To only enter container shell
```
docker build . -t sisdis/homepage:latest && \
docker run -p 5000:80 -it --rm --name ewallet --entrypoint /bin/bash sisdis/homepage:latest
```