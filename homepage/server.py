
# coding: utf-8

# # Python HTTP server
# 
# ## How to run
# Untuk melakukan konfigurasi port, buka file server.py dan ubah argument port pada pemanggilan `MultiThreadedTCPServer`. Kemudian jalankan python server.py untuk menjalankan HTTP server (di folder yang sama dengan resource web).
# 
# ## Things that I've Learned
# 
# - How HTTP protocol works!
# - How to handle multithread connections.
# - How to make abstraction layer over TCP server.
# - etc.
# 
# ## Owner
# Aldi Fahrezi - 1506690624  
# Sistem Terdistribusi

# In[1]:

import socket 
import threading 
  
class MultiThreadedTCPServer:
    def __init__(self, host, port, handler_fn):
        self.host=host
        self.port=port
        self.handler_fn=handler_fn

    def run(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            print("Launching HTTP server on ", self.host, ":", self.port)
            self.socket.bind((self.host, self.port))
        except Exception as e:
            print ("Error: Could not acquire port: ", self.port, "\n")
            self.shutdown()
            return

        print ("Server successfully acquired the socket with port: ", self.port)
        self._listen_for_connections()

    def shutdown(self):
        try:
            print("Shutting down the server")
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception as e:
            print("Warning: could not shut down the socket. Maybe it was already closed?", e)

    def _make_thread_handler(self, conn, addr, io_lock, handler_fn):
        def connection_handler(conn, addr, io_lock, handler_fn):
            while True: 
                # data received from client 
                request_data = conn.recv(1024) 
                if not request_data: 
                    with io_lock: 
                        print('Connection to :', addr[0], ':', addr[1], ' is closed.')
                    break

                with io_lock: 
                    print('Handling request from :', addr[0], ':', addr[1])

                response_data = handler_fn(request_data)
                conn.send(response_data)

            # connection closed 
            conn.close() 

        threaded_handler = threading.Thread(target=connection_handler, args=(conn, addr, io_lock, handler_fn))
        return threaded_handler

    def _listen_for_connections(self):
        self.socket.listen(10)
        print("socket is listening") 

        io_lock = threading.Lock()

        while True: 
            # establish connection with client 
            c, addr = self.socket.accept() 

            # lock acquired by client 
            with io_lock: 
                print('Connected to :', addr[0], ':', addr[1])

            # Start a new thread and return its identifier 
            thread_handler = self._make_thread_handler(c, addr, io_lock, self.handler_fn)
            thread_handler.start()

        self.shutdown()


# In[2]:

class HTTPResponseCode:
    BAD_REQUEST = 400
    METHOD_NOT_ALLOWED = 405
    INTERNAL_SERVER_ERROR = 500
    NOT_IMPLEMENTED = 501
    OK = 200
    NOT_FOUND = 404
    FOUND = 302
    
    DESCRIPTION = {
        BAD_REQUEST: "Bad Request",
        NOT_IMPLEMENTED: "Not Implemented",
        OK: "OK",
        NOT_FOUND: "Not Found",
        FOUND: "Found",
        INTERNAL_SERVER_ERROR: "Internal Server Error",
        METHOD_NOT_ALLOWED: "Method Not Allowed"
    }

class HTTPError(Exception):
    def __init__(self, value, error_message=None, headers=None):
        self.value = value
        self.error_message = error_message
        if headers is None:
            self.headers = dict()
        else:
            self.headers = headers
    def __str__(self):
        return str(self.value) + " " + HTTPResponseCode.DESCRIPTION[self.value]
    
    


# In[3]:

import datetime
import json
import os
import random
import requests
import time
import yaml

from urllib.parse import unquote

def http_request_parser(request_data):
    def _parse_query_string(query_string):
        try:
            if query_string == '':
                return dict()
            
            request_queries = dict()

            key_value_pairs = query_string.split('&')
            for fields in key_value_pairs:
                key, value = fields.split('=')
                request_queries[key] = unquote(value)

            return request_queries
        except:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST)
            
    def _parse_query_json(query_string):
        try:
            if query_string == '':
                return dict()
            
            return json.loads(query_string)
        except:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST)
    
    def _parse_request_line(request_line):
        request_line_tokens = request_line.split(' ')
        if len(request_line_tokens) != 3:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST)
        
        return request_line_tokens[0].upper(), request_line_tokens[1].lower(), request_line_tokens[2].upper()
    
    def _parse_request_headers(request_lines, line_idx):
        request_headers = dict()
                     
        while line_idx < len(request_lines):
            tokens = request_lines[line_idx].split(': ')
            if len(tokens) != 2:
                break
            request_headers[tokens[0].lower()] = tokens[1].strip()            
            line_idx += 1
        
        return request_headers, line_idx
    
    def _parse_get_request_path(data):
        tokens = data.split('?')
        if len(tokens) == 1:
            return tokens[0], dict()
        elif len(tokens) == 2:
            request_path, query_string = tokens[0], tokens[1]
            return request_path, _parse_query_string(query_string)
        else:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST, error_message="Reason: Invalid URL Query Format")
    
    def _parse_post_request_body(data, content_type):
        if content_type == 'application/x-www-form-urlencoded':
            return _parse_query_string(data)
        elif content_type == 'application/json':
            return _parse_query_json(data)
        else:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST)
    
    request_protocol = None
    request_method = None
    request_path = None
    request_headers = None
    request_queries = None
    
    request_data = bytes.decode(request_data)
    request_lines = request_data.split('\r\n')

    # Handle first request line
    request_method, request_path, request_protocol = _parse_request_line(request_lines[0])
    request_headers, request_body_line_idx = _parse_request_headers(request_lines, 1)
    if request_method == 'GET':
        request_path, request_queries = _parse_get_request_path(request_path)
    elif request_method == 'POST':
        if 'content-type' not in request_headers:
            request_headers['content-type'] = 'application/x-www-form-urlencoded'
        request_body_line_idx += 1 # Skip empty line separating headers
        request_queries = _parse_post_request_body(request_lines[request_body_line_idx], request_headers['content-type'])
        
    return {'protocol': request_protocol,
            'method': request_method,
            'path': request_path,
            'headers': request_headers,
            'queries': request_queries}

def build_response(response):
    response['headers']['Content-Length'] = 0 if response['body'] is None else len(response['body'])
    if response['protocol'] is None:
        response['protocol'] = 'HTTP/1.1'
    response['headers']['Connection'] = 'Close'

    response_lines = []
    response_lines.append('{protocol} {code}'.format(protocol=response['protocol'], code=response['code']))
    for k, v in response['headers'].items():
        response_lines.append('{}: {}'.format(k, v))
    
    if response['body'] is not None:
        response_lines.append('')
        response_lines.append(response['body'])
        
    response_lines = [x.encode('utf-8') if type(x) == str else x for x in response_lines]
    
    return (b'\r\n'.join(response_lines))

class ResponseHandler:
    @staticmethod
    def get_api_version():
        y = None
        with open('spesifikasi.yaml', 'r') as f:
            try:
                return yaml.load(f)['info']['version']
            except:
                raise HTTPError(HTTPResponseCode.INTERNAL_SERVER_ERROR)
    
    @staticmethod
    def fetch_static_file_response(response, file_path):
        contents = None
        with open(file_path, 'rb') as f:
            contents = f.read()

        content_type = None
        file_ext = file_path.split('.')[-1]
        if file_ext == 'css':
            content_type = 'text/css; charset=UTF-8'
        elif file_ext == 'html':
            content_type = 'text/html; charset=UTF-8'
        elif file_ext == 'jpg':
            content_type = 'image/jpg'

        response['headers']['Content-Type'] = content_type
        response['body'] = contents

        return response

    @staticmethod
    def hello_world_response(request, response):
        def fetch_dynamic_file_response(response, file_path, find_and_replace_dict):
            response = ResponseHandler.fetch_static_file_response(response, file_path)
            for k, v in find_and_replace_dict.items():
                response['body'] = response['body'].replace(k.encode('UTF-8'), v.encode('UTF-8'))

            return response

        if 'name' in request['queries']:
            response = fetch_dynamic_file_response(response, 'hello-world.html', {'__HELLO__': request['queries']['name']})
        else:
            response = fetch_dynamic_file_response(response, 'hello-world.html', {'__HELLO__': 'World'})

        return response

    @staticmethod
    def info_response(request, response):
        def create_current_time_response(response):
            response['headers']['Content-Type'] = 'text/plain'

            response['body'] = str(datetime.datetime.now()).encode('UTF-8')
            return response

        def create_random_integer_response(response):
            response['headers']['Content-Type'] = 'text/plain'

            response['body'] = str(random.randint(0, 1000000)).encode('UTF-8')
            return response

        def create_no_data_response(response):
            response['headers']['Content-Type'] = 'text/plain'
            response['body'] = "No Data".encode("UTF-8")
            return response

        if 'type' not in request['queries']:
            response = create_no_data_response(response)
        elif request['queries']['type'] == 'time':
            response = create_current_time_response(response)
        elif request['queries']['type'] == 'random':
            response = create_random_integer_response(response)
        else:
            response = create_no_data_response(response)

        return response

    @staticmethod
    def create_plusone_response(response, value):
        response['headers']['Content-Type'] = 'application/json'
        json_response = dict()
        json_response['apiversion'] = ResponseHandler.get_api_version()
        json_response['plusoneret'] = value + 1
        response['body'] = json.dumps(json_response,  sort_keys=True, indent=4, separators=(',', ': '))

        return response

    @staticmethod
    def create_hello_response(response, name):
        def add_and_get_count(key):
            if not os.path.exists("./count-keys"):
                os.makedirs("./count-keys")
            with open('./count-keys/{}'.format(key), 'w') as f:
                pass
            return len([name for name in os.listdir('./count-keys/')])
        
        response['headers']['Content-Type'] = 'application/json'
        r = requests.get('http://172.22.0.222:5000')
        res = r.json()
        
        json_response = dict()
        json_response['response'] = "Good {}, {}".format(res['state'], name)
        count = add_and_get_count(key=str(time.time()))
        json_response['count'] = count
        json_response['apiversion'] = ResponseHandler.get_api_version()
        json_response['currentvisit'] = datetime.datetime.now().isoformat()
        response['body'] = json.dumps(json_response, sort_keys=True, indent=4, separators=(',', ': '))

        return response

def http_request_handler(request_data):
    valid_request_methods = ["GET", "POST"]
    valid_protocol_versions = ["HTTP/1.0", "HTTP/1.1"]

    request = None
    response = dict()
    response['headers'] = dict()
    response['protocol'] = None
    response['code'] = None
    response['body'] = None
    
    try:
        request = http_request_parser(request_data)
        
        if request['method'] not in valid_request_methods:
            raise HTTPError(HTTPResponseCode.NOT_IMPLEMENTED, error_message="Reason: {}".format(request['method']))
        if request['protocol'] not in valid_protocol_versions:
            raise HTTPError(HTTPResponseCode.BAD_REQUEST, error_message="Reason: Unsupported Protocol")           
        response['protocol'] = request['protocol']
        
        # Route handler
        if request['method'] == 'GET':
            if request['path'] == '/':
                raise HTTPError(HTTPResponseCode.FOUND, headers={
                    'Location': '/hello-world'
                }, error_message="Location: /hello-world")
            if request['path'] == '/style':
                response = ResponseHandler.fetch_static_file_response(response, 'style.css')
            elif request['path'] == '/background':
                response = ResponseHandler.fetch_static_file_response(response, 'background.jpg')
            elif request['path'] == '/hello-world':
                response = ResponseHandler.hello_world_response(request, response)
            elif request['path'] == '/info':
                response = ResponseHandler.info_response(request, response)
            elif request['path'] == '/api/spesifikasi.yaml':
                response = ResponseHandler.fetch_static_file_response(response, 'spesifikasi.yaml')
            elif request['path'] == '/api/hello':
                raise HTTPError(HTTPResponseCode.METHOD_NOT_ALLOWED)
            elif request['path'][:13] == '/api/plus_one':
                value = None
                try:
                    value = int(request['path'].split('/')[-1])
                except:
                    raise HTTPError(HTTPResponseCode.NOT_FOUND)
                response = ResponseHandler.create_plusone_response(response, value)
            else:
                raise HTTPError(HTTPResponseCode.NOT_FOUND)
        elif request['method'] == 'POST':
            if request['path'] == '/hello-world':
                response = ResponseHandler.hello_world_response(request, response)
            elif request['path'] == '/api/spesifikasi.yaml':
                response = ResponseHandler.fetch_static_file_response(response, 'spesifikasi.yaml')
            elif request['path'] == '/api/hello':
                if 'request' not in request['queries']:
                    raise HTTPError(HTTPResponseCode.BAD_REQUEST, error_message="'request' is a required property")
                response = ResponseHandler.create_hello_response(response, request['queries']['request'])
            elif request['path'][:13] == '/api/plus_one':
                value = None
                try:
                    value = int(request['path'].split('/')[-1])
                except:
                    raise HTTPError(HTTPResponseCode.NOT_FOUND)
                raise HTTPError(HTTPResponseCode.METHOD_NOT_ALLOWED)
            else:
                raise HTTPError(HTTPResponseCode.NOT_FOUND)
        
        response['code'] = str(HTTPError(HTTPResponseCode.OK))
        
    except HTTPError as e:
        response['code'] = str(e)
        response['headers'] = e.headers
        if 'Content-Type' not in response['headers']:
            if request['path'][:4] == "/api":
                response['headers']['Content-Type'] = "application/json"
            else:
                response['headers']['Content-Type'] = "text/plain; charset=UTF-8"
        if response['headers']['Content-Type'][:10] == "text/plain":
            if e.error_message is not None:
                response['body'] = "{}: {}".format(str(e), e.error_message).encode("UTF-8")
            else:
                response['body'] = str(e).encode("UTF-8")
        elif response['headers']['Content-Type'] == "application/json":
            json_response = dict()
            if e.error_message is not None:
                json_response['detail'] = e.error_message
            elif e.value == HTTPResponseCode.NOT_FOUND:
                json_response['detail'] = "The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again."
            else:
                json_response['detail'] = "-"
            json_response['status'] = e.value
            json_response['title'] = HTTPResponseCode.DESCRIPTION[e.value]
            response['body'] = json.dumps(json_response, sort_keys=True, indent=4, separators=(',', ': '))
            
    except Exception as e:
        print(e)
        response['code'] = str(HTTPError(HTTPResponseCode.INTERNAL_SERVER_ERROR))
        response['headers'] = dict()
        response['headers']['Content-Type'] = "text/plain; charset=UTF-8"
        response['body'] = "{}: Reason: unknown".format(response['code']).encode("UTF-8")
        
    return build_response(response)    


# In[4]:

if __name__ == "__main__":
    print ("Starting web server")
    if 'HOMEPAGE_DEPLOYMENT_PORT' not in os.environ:
        server = MultiThreadedTCPServer(host='', port=80, handler_fn=http_request_handler)
    else:
        server = MultiThreadedTCPServer(host='', port=int(os.environ['HOMEPAGE_DEPLOYMENT_PORT']), handler_fn=http_request_handler)
    server.run()

# In[ ]:
